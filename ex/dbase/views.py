# Create your views here.
from datetime import datetime, timedelta
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
import hashlib
 
from dbase import models as m
from django.template import loader, Context
from dbase.forms import CategoryForm,UserForm, GenHashForm, BruteForceForm, ZadaniaForm,SlownikowaForm
from django.template.context import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import logout
from django.contrib.auth.decorators import login_required
from dbase.models import Zadania

import json
from django.http import JsonResponse
from django.core import serializers


def index(request):
        recent = m.Computer.objects.all()
        
        context = Context({
            'Computer_list': recent
        })
        # Render accepts three arguments: the request object, the
        # path of the template file and the context
        return render(request, 'index.html', context)




@login_required
def str(request):
    recent =m.HostGroup.objects.all()
    context = Context({
        'HostGroup_list': recent
    })
    # Render accepts three arguments: the request object, the
    # path of the template file and the context
    return render(request, 'str.html', context)

@login_required
def stronka(request):
    two_days_ago = datetime.utcnow() - timedelta(days=2)
    recent =m.HostGroup.objects.all()
    
    context = Context({
        'HostGroup_list': recent
    })
    # Render accepts three arguments: the request object, the
    # path of the template file and the context
    return render(request, 'stronka.html', context)

@login_required
def kategorie(request):
    two_days_ago = datetime.utcnow() - timedelta(days=2)
    recent =m.Category.objects.all()
    
    context = Context({
        'Category_list': recent
    })
    # Render accepts three arguments: the request object, the
    # path of the template file and the context
    return render(request, 'kategorie.html', context)

@login_required
def glowna(request):
    two_days_ago = datetime.utcnow() - timedelta(days=2)
    recent =m.Category.objects.all()
    
    context = Context({
        'Category_list': recent
    })
    # Render accepts three arguments: the request object, the
    # path of the template file and the context
    return render(request, 'glowna.html', context)


@login_required
def metoda_bruteforce(request):
    # Get the context from the request.
    context = RequestContext(request)
   

    # A HTTP POST?
    if request.method == 'POST':
        form = BruteForceForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
            zad= form.save(commit=False)
            # commit=False tells Django that "Don't send this to database yet.
            # I have more things I want to do with it."
            zad.type = 'bruteforce'
            alfabet=0
            LowerCase = form.cleaned_data['LowerCase']
            UpperCase = form.cleaned_data['UpperCase']
            Digits = form.cleaned_data['Digits']
            specialCharacters = form.cleaned_data['specialCharacters']
            
            if LowerCase == 'true':
                alfabet= alfabet+8
            if UpperCase == 'true':
                alfabet= alfabet+4
            if Digits == 'true':
                alfabet= alfabet+2
            if specialCharacters == 'true':
                alfabet= alfabet+1
            # commit=False tells Django that "Don't send this to database yet.
            # I have more things I want to do with it."
            #print alfabet
            zad.alphabet=alfabet
            zad.owner = request.user # Set the user object here
            zad.save()

            # Now call the index() view.
            # The user will be shown the homepage.
            #return  render(request, 'historia.html',{'form': form})
            return  redirect('/dbase/historia',{'form': form})
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
       form = BruteForceForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render_to_response('metoda_bruteforce.html',{'form': form}, context)


@login_required
def Zadanie_widok(request):
    # Get the context from the request.
    context = RequestContext(request)
    zadania_list = Zadania.objects.filter(owner=request.user)
    context_dict = {'Zlecenia': zadania_list}
    # A HTTP POST?
    if request.method == 'POST':
        form = ZadaniaForm(request.POST)
        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
           # zad = form.save(commit=True)
            zad= form.save(commit=False)
            alfabet = form.cleaned_data['alfabet2']
            # commit=False tells Django that "Don't send this to database yet.
            # I have more things I want to do with it."
            print alfabet
            zad.alphabet=44
            zad.owner = request.user # Set the user object here
            zad.save()
            # Now call the index() view.
            # The user will be shown the homepage.
            return  render(request, 'glowna.html')
            #return redirect('/dbase/historia.html',{'form': form})
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
       form = ZadaniaForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render_to_response('zadaj_zadanie.html',{'form': form}, context)


def get_tasks(request):
    tasks = Zadania.objects.filter(owner=request.user)
    items = serializers.serialize('json', tasks)
    result = json.dumps(items)
    return HttpResponse(result, content_type="application/json")

def delFromDB(request):
    result = json.dumps(request.GET)
    data = json.loads(result)
    task = Zadania.objects.filter(id=data['id']).delete()
    return HttpResponse(result, content_type="application/json")

def delAllFromDB(request):
    Zadania.objects.filter(owner=request.user).delete() 

   # context = RequestContext(request)
  #  zadania_list = Zadania.objects.filter(owner=request.user)
 #   context_dict = {'Zlecenia': zadania_list}
#    return render_to_response('historia.html', context_dict, context)

def Historia_zlecen(request):
    # Obtain the context from the HTTP request.
    context = RequestContext(request)

   
    zadania_list = Zadania.objects.filter(owner=request.user)
    context_dict = {'Zlecenia': zadania_list}
    
    
     
    #context_dict = {'Zlecenia': zadania_list }
    
     # The following two lines are new.
    # We loop through each category returned, and create a URL attribute.
    # This attribute stores an encoded URL (e.g. spaces replaced with underscores).
    #for category in category_list:
    #    category.url = category.name.replace(' ', '_')

    # Render the response and send it back!
    return render_to_response('historia.html', context_dict, context)



 



def metoda_slownikowa(request):
     # Get the context from the request.
    context = RequestContext(request)
    
    zadania_list = Zadania.objects.filter(owner=request.user).order_by('-id')[:1]
    #zadania_list = Zadania.objects.values_list('id').order_by('-id')[:1]
    context_dict = {'Zlecenia': zadania_list}
    
    # A HTTP POST?
    if request.method == 'POST':
        form = SlownikowaForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
            zad= form.save(commit=False)
            # commit=False tells Django that "Don't send this to database yet.
            # I have more things I want to do with it."
            zad.type = 'dictionary'
            zad.status = 'WAITING'
            zad.password_length=0
            # commit=False tells Django that "Don't send this to database yet.
            # I have more things I want to do with it."
            #print alfabet
            
            zad.owner = request.user # Set the user object here
            zad.save()

            # Now call the index() view.
            # The user will be shown the homepage.
            return  render(request, 'glowna.html',{'form': form})
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
       form = SlownikowaForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    #return render_to_response('metoda_slownikowa.html',{'form': form},context_dict, context)
   
    return render_to_response('metoda_slownikowa.html',{'form': form}, context)

def generator_hash(request):
    if request.method == 'GET':
        form = GenHashForm()
    else:
        form = GenHashForm(request.POST)
        if form.is_valid():
            # Save the new category to the database.
            form.save(commit=True)
            #content = form.cleaned_data['content']
            #created_at = form.cleaned_data['created_at']
            #post = m.Post.objects.create(content=content,created_at=created_at)
            haslo = form.cleaned_data['haslo']
            metoda = form.cleaned_data['metoda']
            if metoda == "md5":
            #context = RequestContext(request)
                hash_object = hashlib.md5(haslo)
                outputPassword1= hash_object.hexdigest()
                return  render(request, 'generator_hash.html',{'form': form, 'outputPassword1': outputPassword1})
            else: 
                if metoda == "sha256" :
                    hash_object=hashlib.sha256(haslo)
                    outputPassword2= hash_object.hexdigest()
                    return  render(request, 'generator_hash.html',{'form': form, 'outputPassword2': outputPassword2})
                else:
                    hash_object = hashlib.md5(haslo)
                    outputPassword1= hash_object.hexdigest()
                    
                    hash_object=hashlib.sha256(haslo)
                    outputPassword2= hash_object.hexdigest()
                    return  render(request, 'generator_hash.html',{'form': form, 'outputPassword1': outputPassword1, 'outputPassword2': outputPassword2})
    #context.__setattr__('outputPassword',form.haslo)

     
    return render(request, 'generator_hash.html',{'form': form })

@login_required
def add_category(request):
    # Get the context from the request.
    context = RequestContext(request)

    # A HTTP POST?
    if request.method == 'POST':
        form = CategoryForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
            form.save(commit=True)

            # Now call the index() view.
            # The user will be shown the homepage.
            return index(request)
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
        form = CategoryForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render_to_response('add_category.html', {'form': form}, context)

def register(request):
    # Like before, get the request's context.
    context = RequestContext(request)

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid() :
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set commit=False.
            # This delays saving the model until we're ready to avoid integrity problems.


            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.


            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print user_form.errors

    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()

    # Render the template depending on the context.
    return render_to_response(
            'register.html',
            {'user_form': user_form,  'registered': registered},
            context)
  
def user_login(request):
    # Like before, obtain the context for the user's request.
    context = RequestContext(request)

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/dbase/glowna')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your  account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('login.html', {}, context)
    
    from django.contrib.auth import logout

# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/dbase/')

@login_required
def restricted(request):
    return HttpResponse("Since you're logged in, you can see this text!")
