'''
Created on 24-04-2015

@author: mateusz
'''

from django.conf.urls import patterns, url
from dbase import views

urlpatterns = patterns('',
    url(r'^$', 'dbase.views.index', name='index'),
    url(r'^stronka/', 'dbase.views.stronka', name='stronka'),
    url(r'^str/', 'dbase.views.str', name='str'),
    url(r'^kategorie/', 'dbase.views.kategorie', name='kategorie'),
    url(r'^add_category/$', views.add_category, name='add_category'), # NEW MAPPING!
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^restricted/', views.restricted, name='restricted'),
    url(r'^glowna/', views.glowna, name='glowna'),
    url(r'^generator/', views.generator_hash, name='generator_hash'),
    url(r'^bruteforce/', views.metoda_bruteforce, name='metoda_bruteforce'),
    url(r'^slownikowa/', views.metoda_slownikowa, name='metoda_slownikowa'),
    url(r'^zadanie/', views.Zadanie_widok, name='zlecanie_zadania'),
    url(r'^historia/', views.Historia_zlecen, name='Historia'),
    url(r'^get_tasks/', views.get_tasks, name='get_tasks'),
    url(r'^delFromDB/', views.delFromDB, name='delFromDB'),
    url(r'^delAllFromDB/', views.delAllFromDB, name='delAllFromDB')
 )

    
