from django.db import models
from django.contrib.auth.models import User
from django.db.backends.oracle.creation import PASSWORD


class HostGroup(models.Model):
    name = models.CharField(max_length=60,unique = True)
    active = models.DateTimeField('date created')

    def __unicode__(self):
        return self.name

class Computer(models.Model):
    name = models.CharField(max_length=60,unique = True)
    host_group = models.ForeignKey(HostGroup)
    active = models.BooleanField()
    active_created = models.DateTimeField('date created')
    
    def __unicode__(self):
        return self.name
    
class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.name

class Page(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=128)
    url = models.URLField()
    views = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title
    
class Haslo(models.Model):
    haslo = models.CharField(max_length=128, unique=True)
    metoda = models.CharField(max_length=128)

    def __unicode__(self):
        return self.metoda

bruteforce_enum = (
              ('LowerCase','LowerCase'),
              ('UpperCase','UpperCase'),
              ('Digits','Digits'),
              ('specialCharacters','specialCharacters')
              )

hash_enum = (
              ('md5','md5'),
              ('sha256','sha256'),
              ('both','both')
              )

true_enum = (
              ('true','true'),
              ('false','false')
              )

class BruteForce(models.Model):
    hashh= models.CharField(max_length=128, unique=True)
    rozmiar = models.IntegerField(null=True, blank=True)
    max_dlu = models.IntegerField(null=True, blank=True)
    LowerCase = models.CharField(choices=true_enum, max_length=128)
    UpperCase = models.CharField(choices=true_enum, max_length=128)
    Digits = models.CharField(choices=true_enum, max_length=128)
    specialCharacters = models.CharField(choices=true_enum, max_length=128)
    metoda = models.CharField(choices=hash_enum, max_length=128)

    

    def __unicode__(self):
        return self.nazwa
    
class Metoda(models.Model):
    nazwa = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.nazwa
    
class TrybHash(models.Model):
    nazwa = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.nazwa
    
class Alfabet(models.Model):
    nazwa = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.nazwa
    
class MaxDlHash(models.Model):
    nazwa = models.CharField(max_length=128, unique=True)
    rozmiar = models.IntegerField(unique = True)

    def __unicode__(self):
        return self.nazwa
    
class RozmiarPojZadania(models.Model):
    nazwa = models.CharField(max_length=128, unique=True)
    rozmiar = models.IntegerField(unique = True)

    def __unicode__(self):
        return self.nazwa



STATUS_ENUM = (
        ('WAITING', 'WAITING'),
        ('FINISHED', 'FINISHED'),
        ('IN_PROGRESS', 'IN_PROGRESS'),
        ('ERROR', 'ERROR'),
    )


type_enum = (
        ('bruteforce','bruteforce'),
        ('dictionary','dictionary'),
        )


alfabet_enum = (
              ('LowerCase','LowerCase'),
              ('UpperCase','UpperCase'),
              ('Digits','Digits'),
              ('specialCharacters','specialCharacters')
              )

hash_enum = (
              ('md5','md5'),
              ('sha256','sha256'),
              ('both','both')
              )

                  
    
    
class Zadania (models.Model):
 
    owner = models.CharField(max_length=30)
    
  
    status = models.CharField(choices=STATUS_ENUM, null = True, max_length=128)#,default=1)
    PASSWORD = models.CharField(max_length=64)
    result =models.CharField(max_length=50)
    time = models.IntegerField(max_length=10,default=0)
    progress = models.IntegerField(max_length=5,default=0)
    
    type = models.CharField(choices=type_enum, max_length=128)
    hash = models.CharField(choices=hash_enum, max_length=128)
    work_size = models.IntegerField(max_length=8)
    password_length = models.IntegerField(max_length=3)
    alphabet = models.IntegerField(max_length=2,null = True)
    
    def __unicode__(self):
        return self.name
    
        
    
