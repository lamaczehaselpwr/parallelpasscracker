'''
Created on 24-04-2015

@author: mateusz
'''
from django import forms
from dbase.models import Page, Category, Haslo, BruteForce, Zadania 
from django.contrib.auth.models import User
from django import test

class CategoryForm(forms.ModelForm):
    name = forms.CharField(max_length=128, help_text="Please enter the category name.")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    # An inline class to provide additional information on the form.
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Category
	fields = "__all__" 

class PageForm(forms.ModelForm):
    title = forms.CharField(max_length=128, help_text="Please enter the title of the page.")
    url = forms.URLField(max_length=200, help_text="Please enter the URL of the page.")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        # Provide an association between the ModelForm and a model
        model = Page

        # What fields do we want to include in our form?
        # This way we don't need every field in the model present.
        # Some fields may allow NULL values, so we may not want to include them...
        # Here, we are hiding the foreign key.
        fields = ('title', 'url', 'views')
        
    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        # If url is not empty and doesn't start with 'http://', prepend 'http://'.
        if url and not url.startswith('http://'):
            url = 'http://' + url
            cleaned_data['url'] = url

        return cleaned_data
    
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        
hash_enum = (
              ('md5','md5'),
              ('sha256','sha256'),
              ('both','both')
              )
        
class GenHashForm(forms.ModelForm):
    haslo = forms.CharField(max_length=128, help_text="Prosze wprowadzic haslo")
    #metoda= forms.MultipleChoiceField(required=True,widget=forms.CheckboxSelectMultiple, choices=hash_enum )
    metoda = forms.ChoiceField(choices=hash_enum)
    
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Haslo
        fields = ('haslo', 'metoda')
    
             
        
bruteforce_enum = (
              ('LowerCase','LowerCase'),
              ('UpperCase','UpperCase'),
              ('Digits','Digits'),
              ('specialCharacters','specialCharacters')
              )

STATUS_ENUM = (
        ('WAITING', 'WAITING'),
        ('FINISHED', 'FINISHED'),
        ('IN_PROGRESS', 'IN_PROGRESS'),
        ('ERROR', 'ERROR'),
    )

type_enum = (
        ('bruteforce','bruteforce'),
        ('dictionary','dictionary'),
        )


alfabet_enum = (
              ('LowerCase','LowerCase'),
              ('UpperCase','UpperCase'),
              ('Digits','Digits'),
              ('specialCharacters','specialCharacters')
              )

true_enum = (
              ('true','true'),
              ('false','false')
              )

hash_enum = (
              ('md5','md5'),
              ('sha256','sha256'),
              ('both','both')
              )
        
class BruteForceForm(forms.ModelForm):

    type = forms.ChoiceField(choices=type_enum, widget=forms.HiddenInput(),help_text="Wybierz metode lamania",initial='bruteforce')

    PASSWORD = forms.CharField(max_length=64, help_text="Prosze wprowadzic haslo")

    

    work_size = forms.IntegerField( help_text="Rozmiar pojedynczego zadania")
    password_length = forms.IntegerField( help_text="Maksymalan dl hasla")
    LowerCase = forms.ChoiceField(choices=true_enum, help_text="LowerCase")
    UpperCase = forms.ChoiceField(choices=true_enum, help_text="UpperCase")
    Digits = forms.ChoiceField(choices=true_enum, help_text="Digits")
    specialCharacters = forms.ChoiceField(choices=true_enum, help_text="specialCharacters")
    hash = forms.ChoiceField(choices=hash_enum,help_text ="Metoda hashowania")
    status = forms.ChoiceField(choices=STATUS_ENUM, widget=forms.HiddenInput(), initial='WAITING')
   # owner = forms.CharField(widget=forms.HiddenInput(),initial = {'user': request.user})
   # alfabet2 = forms.ChoiceField(widget=forms.HiddenInput(),choices=bruteforce_enum)
    #owner = forms.CharField(widget=forms.HiddenInput())
    
    class Meta:
        model= Zadania
        # Provide an association between the ModelForm and a model
        fields = ( 'type','PASSWORD','work_size','password_length','status','hash','LowerCase','UpperCase','Digits','specialCharacters')


    
class SlownikowaForm(forms.ModelForm):
    
    #type = forms.ChoiceField(choices=type_enum,help_text="Wybierz metode lamania")

    PASSWORD = forms.CharField(max_length=64, help_text="Prosze wprowadzic haslo")
    work_size = forms.IntegerField( help_text="Rozmiar pojedynczego zadania")
    hash = forms.ChoiceField(choices=hash_enum,help_text ="Metoda hashowania")
    #status = forms.ChoiceField(choices=STATUS_ENUM, widget=forms.HiddenInput(), initial='WAITING')
    
    
    class Meta:
        model= Zadania
        # Provide an association between the ModelForm and a model
        fields = ( 'PASSWORD','work_size','hash')
        


class ZadaniaForm(forms.ModelForm):
    
    type = forms.ChoiceField(choices=type_enum,help_text="Wybierz metode lamania")

    PASSWORD = forms.CharField(max_length=64, help_text="Prosze wprowadzic haslo")

    

    work_size = forms.IntegerField( help_text="Rozmiar pojedynczego zadania")
    password_length = forms.IntegerField( help_text="Maksymalan dl hasla")
    #alphabet = forms.IntegerField(help_text="Alfabet")
    #alphabet = forms.IntegerField(widget=forms.HiddenInput(),help_text="Alfabet")
    hash = forms.ChoiceField(choices=hash_enum,help_text ="Metoda hashowania")
    status = forms.ChoiceField(choices=STATUS_ENUM, widget=forms.HiddenInput(), initial='WAITING')
    alfabet2 = forms.ChoiceField(widget=forms.HiddenInput(),choices=bruteforce_enum)
   # owner = forms.CharField(widget=forms.HiddenInput(),initial = {'user': request.user})
    #alfabet = forms.ChoiceField(choices=bruteforce_enum)
    #owner = forms.CharField(widget=forms.HiddenInput())
    
    class Meta:
        model= Zadania
        # Provide an association between the ModelForm and a model
        fields = ('type', 'PASSWORD','work_size','password_length','status','hash','alfabet2')

