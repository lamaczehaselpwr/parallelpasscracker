'''
Created on 20-04-2015

@author: mateusz
'''
from dbase.models import HostGroup, Computer, Category, Page, Haslo, Metoda,\
    TrybHash, Alfabet, MaxDlHash, RozmiarPojZadania
from django.contrib import admin
admin.site.register(Computer)
admin.site.register(HostGroup)
admin.site.register(Category)
admin.site.register(Page)
admin.site.register(Haslo)
admin.site.register(Metoda)
admin.site.register(TrybHash)
admin.site.register(Alfabet)
admin.site.register(MaxDlHash)
admin.site.register(RozmiarPojZadania)
