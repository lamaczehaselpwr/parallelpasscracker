
#include <mysql++/mysql++.h>
#include <iostream>
#include <unistd.h>
#include <string>
#include <unistd.h>
#include <sstream>

#define DATABASE_NAME "mpi"
#define DATABASE_HOST "jaracza.noip.me"
#define DATABASE_USER "mpi"
#define DATABASE_PASS "mpi"

struct Task
{
			unsigned id;
			std::string owner;
			std::string status;
			std::string password;
			std::string type;
			std::string hash;
			int work_size;
			int password_length;
			int alphabet;
};

//database functions
void findTask(Task *task);
void changeTaskStatus(std::string status, unsigned id);
void updateProgress(unsigned progress, unsigned id);
//other functions
int runMPI(Task *task);

int main()
{
	while(1)
	{
		struct Task task{};
		while(task.id == 0)
		{
			sleep(5);
			std::cout<<"Looking for a task...\n";
			findTask(&task);
		}
		std::cout<<"Starting task with ID = "<<task.id<<" commissioned by "<<task.owner<<"\n";
		changeTaskStatus("in progress", task.id);
		std::cout<<"Task("<<task.id<<") status changed to 'in progress'\n";
		if( !runMPI(&task))
		{
			changeTaskStatus("finished", task.id);
			std::cout<<"Task("<<task.id<<") status changed to 'finished'\n";
		}
		else
		{
			changeTaskStatus("error", task.id);
			std::cout<<"Task("<<task.id<<") status changed to 'error'\n";
		}
		
	}
	return 0;
}

void findTask(Task *task)
{
	using namespace mysqlpp;
	using namespace std;
	
	try 
	{
        Connection conn(false);
        conn.connect(DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PASS);
        Query query = conn.query();
		query << "SELECT * FROM dbase_zadania WHERE status = 'waiting' LIMIT 1";
        StoreQueryResult ares = query.store();
		if(ares.num_rows() == 1)
		{
			task->id = atoi(ares[0]["ID"].c_str());
			task->owner = ares[0]["owner"].c_str();
			task->status = ares[0]["status"].c_str();
			task->password = ares[0]["password"].c_str();
			task->type = ares[0]["type"].c_str();
			task->hash = ares[0]["hash"].c_str();
			task->work_size = atoi(ares[0]["work_size"]);
			task->password_length = atoi(ares[0]["password_length"]);
			task->alphabet = atoi(ares[0]["alphabet"]);
		}
    } 	catch (BadQuery er) 
		{ 	// handle any connection or
			// query errors that may come up
			cerr << "Error: " << er.what() << endl;
		} 
		catch (const BadConversion& er) 
		{
			// Handle bad conversions
			cerr << "Conversion error: " << er.what() << endl <<
					"\tretrieved data size: " << er.retrieved <<
					", actual size: " << er.actual_size << endl;
		} 
		catch (const Exception& er) 
		{
			// Catch-all for any other MySQL++ exceptions
			cerr << "Error: " << er.what() << endl;
		}
}

void changeTaskStatus(std::string status, unsigned id)
{
	using namespace mysqlpp;
	using namespace std;
	
	try 
	{
        Connection conn(false);
        conn.connect(DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PASS);
        Query query = conn.query();
		query << "UPDATE dbase_zadania SET status='"<<status<<"' WHERE ID='"<<id<<"'";
        StoreQueryResult ares = query.store();
    } 	catch (BadQuery er) 
		{ 	// handle any connection or
			// query errors that may come up
			cerr << "Error: " << er.what() << endl;
		} 
		catch (const BadConversion& er) 
		{
			// Handle bad conversions
			cerr << "Conversion error: " << er.what() << endl <<
					"\tretrieved data size: " << er.retrieved <<
					", actual size: " << er.actual_size << endl;
		} 
		catch (const Exception& er) 
		{
			// Catch-all for any other MySQL++ exceptions
			cerr << "Error: " << er.what() << endl;
		}
}

void updateProgress(unsigned progress, unsigned id)
{
	using namespace mysqlpp;
	using namespace std;
	
	try 
	{
        Connection conn(false);
        conn.connect(DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PASS);
        Query query = conn.query();
		query << "UPDATE dbase_zadania SET progress='"<<progress<<"' WHERE ID='"<<id<<"'";
        StoreQueryResult ares = query.store();
    } 	catch (BadQuery er) 
		{ 	// handle any connection or
			// query errors that may come up
			cerr << "Error: " << er.what() << endl;
		} 
		catch (const BadConversion& er) 
		{
			// Handle bad conversions
			cerr << "Conversion error: " << er.what() << endl <<
					"\tretrieved data size: " << er.retrieved <<
					", actual size: " << er.actual_size << endl;
		} 
		catch (const Exception& er) 
		{
			// Catch-all for any other MySQL++ exceptions
			cerr << "Error: " << er.what() << endl;
		}	
}

int runMPI(Task *task)
{
	std::ostringstream ss;
    ss << "mpirun --hostfile hostfile haslo " << task->id << " " << task->password << " " << task->type << " " << task->hash;
	ss << " " << task->work_size << " " << task->password_length << " " << task->alphabet;
	std::string command = ss.str();
	std::cout<<command<<"\n";
	return system(command.c_str());
}
