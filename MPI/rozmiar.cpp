#include <iostream>
#include <fstream>
#include <string>

int main()
{
	std::fstream plik;
	plik.open("dictionary.txt",std::ios::in);
	if(plik.good() == true)
	{
		std::string linia;
		unsigned long counter = 0;
		unsigned long show = 10000000;
		while( !plik.eof() )
		{
			plik >> linia;
			counter++;
			if(counter == show)
			{
				show += 10000000;
				std::cout<<counter<<"\n";
			}
		}
		std::cout<<"Liczba lini w slowniku wynosi: "<<counter<<"\n";
		plik.close();
	}
	else
	{
		std::cerr<<"Nie mozna otworzyc pliku!\n";
	}
	return 0;
}
