#include <string>
class Alphabet
{
private:
std::string alphabet;

public:
  Alphabet();
  Alphabet(int x);
  ~Alphabet();
  
  void initialize(int x);
  char operator[](int element);
  int getLength();
  int findCharacter(char character);
  
};


