#include "md5.h"
#include "alphabet.h"
#include <mysql++/mysql++.h>
#include <mpi.h>
#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <openssl/sha.h>
#include <sstream>
#include <iomanip>
#include <time.h>
#include <string>
#include <math.h>
#include <fstream>
#include <vector>

#define WORKTAG 1
#define DIETAG  2
#define MAX_DICTIONARY_PASSWORD 256

#define DATABASE_NAME "mpi"
#define DATABASE_HOST "jaracza.noip.me"
#define DATABASE_USER "mpi"
#define DATABASE_PASS "mpi"

const std::string dictionaryPath("dictionary.txt");
long long MAX_BRUTEFORCE_JOBS;
long long MAX_DICTIONARY_JOBS;
int ID;
std::string PASSWORD;
std::string BRUTEFORCE;
std::string HASH;
int WORK_SIZE;
int DICTIONARY_SIZE;
int BRUTEFORCE_SIZE;
Alphabet alphabet(0);
char BUFFER[21];


void bruteforceMaster();
void dictionaryMaster();
void bruteforceSlave();
void dictionarySlave();
std::string nextPermutation(std::string string);
std::string getAlphabetLetter(int element);
std::string xthPermutation(long long task);
void updateProgress(unsigned progress);
void setResult(std::string result, int time);
std::string sha256(const std::string str);

int main(int argc, char *argv[])
{
	ID = atoi(argv[1]);
	PASSWORD = argv[2];
	BRUTEFORCE = argv[3];
	HASH = argv[4];
	WORK_SIZE = DICTIONARY_SIZE = atoi(argv[5]);
	BRUTEFORCE_SIZE = atoi(argv[6]);
	if(BRUTEFORCE == "bruteforce")
	{
		alphabet.initialize(atoi(argv[7]));
	}
	
	
	MAX_BRUTEFORCE_JOBS = pow(alphabet.getLength(),BRUTEFORCE_SIZE)/WORK_SIZE;
	MAX_DICTIONARY_JOBS = ceil(1327019198/WORK_SIZE);
    int myRank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    
    if(myRank ==0)
    {
		if(BRUTEFORCE == "bruteforce")
			bruteforceMaster();
		else
			dictionaryMaster();
    }
    else
    {
		if(BRUTEFORCE == "bruteforce")
			bruteforceSlave();
		else
			dictionarySlave();
    }

    MPI_Finalize();
    return 0;
}

void bruteforceMaster()
{
    time_t begin = time(0);
	clock_t hangStart;
	long long hangTotal = 0;
    int ntasks, rank, work = 1;
    std::string stringToCheck = "x"; 
    stringToCheck[0] = alphabet[0];
    std::string result = "";
    char buffor[BRUTEFORCE_SIZE+1];
	int workStatus = 0;
	long long task = 0;
	int nextWorkStatus = 0;
    MPI_Status status;
	
	
    for(int i=0;i<BRUTEFORCE_SIZE+1;i++)
      buffor[i] = '\0';

    MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
    
    for(rank = 1; rank < ntasks; ++rank)
    {
      if(task <= pow(alphabet.getLength(),BRUTEFORCE_SIZE))
	{
	  MPI_Send(&task,1,MPI_INT,rank,WORKTAG,MPI_COMM_WORLD);
	  std::cout<<"Zlecono sprawdzenie od pozycji "<<task<<" job "<<work<<" of "<<MAX_BRUTEFORCE_JOBS<<" ("<<(double)work/MAX_BRUTEFORCE_JOBS*100<<"%)"<<"\n";
	  nextWorkStatus = (int)((double)work/MAX_BRUTEFORCE_JOBS*10000);
	  if(nextWorkStatus > workStatus)
	  {
		  workStatus = nextWorkStatus;
		  updateProgress(workStatus);
	  }
	  task += WORK_SIZE;
	  work++;
	}
 
    }
    while (result == "")
    {
		hangStart = clock();
        MPI_Recv(&buffor,BRUTEFORCE_SIZE,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
		result = std::string(buffor);
		hangTotal += clock()-hangStart;
		
		for(int i=0;i<BRUTEFORCE_SIZE+1;i++)
			buffor[i] = '\0';

		if(result != "")
			break;

        if(task <= pow(alphabet.getLength(),BRUTEFORCE_SIZE))
		{
			MPI_Send(&task,1,MPI_INT, status.MPI_SOURCE, WORKTAG, MPI_COMM_WORLD);
			std::cout<<"Zlecono sprawdzenie od pozycji "<<task<<" job "<<work<<" of "<<MAX_BRUTEFORCE_JOBS<<" ("<<(double)work/MAX_BRUTEFORCE_JOBS*100<<"%)"<<"\n";
			nextWorkStatus = (int)((double)work/MAX_BRUTEFORCE_JOBS*10000);
		    if(nextWorkStatus > workStatus)
		    {
			    workStatus = nextWorkStatus;
			    updateProgress(workStatus);
		    }
			task += WORK_SIZE;
			work++;
		}
		else
			break;
    }
    if(result == "")
    {
      for(rank = 1; rank <ntasks-1; ++rank)
      {
        MPI_Recv(&buffor, BRUTEFORCE_SIZE, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		result = std::string(buffor);
		if(result != "")
			break;
		for(int i=0;i<BRUTEFORCE_SIZE+1;i++)
			buffor[i] = '\0';
      }
    }
    for(rank = 1; rank < ntasks; ++rank)
    {
        MPI_Send(0,0,MPI_INT,rank,DIETAG, MPI_COMM_WORLD);
    }
    if(result != "")
	{
      std::cout<<"SOLVED!!! PASSWORD IS "<<result<<"!!!!\n";
	  setResult(result, difftime(time(0), begin));
	}
    else
      std::cout<<"PASSWORD NOT FOUND!\n";
    std::cout<<"It took "<<difftime(time(0), begin)<<"s to check.\n";
	std::cout<<"Master had to wait "<<hangTotal/CLOCKS_PER_SEC<<"s\n";
    
}

void dictionaryMaster()
{
	time_t begin = time(0);
    int ntasks, rank, work = 1;
	long long task = 0;
    std::string result = "";
	int workStatus = 0;
	int nextWorkStatus = 0;
    MPI_Status status;
	char buffor[MAX_DICTIONARY_PASSWORD];

    for(int i=0;i<MAX_DICTIONARY_PASSWORD;i++)
      buffor[i] = '\0';
    
    MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
    
    for(rank = 1; rank < ntasks; ++rank)
    {
	  MPI_Send(&task,1,MPI_INT,rank,WORKTAG,MPI_COMM_WORLD);
	  std::cout<<"Zlecono sprawdzenie od pozycji "<<task<<" job "<<work<<" of "<<MAX_DICTIONARY_JOBS<<" ("<<(double)work/MAX_DICTIONARY_JOBS*100<<"%)"<<"\n";
	  nextWorkStatus = (int)((double)work/MAX_DICTIONARY_JOBS*10000);
	  if(nextWorkStatus > workStatus)
	  {
		  workStatus = nextWorkStatus;
		  updateProgress(workStatus);
	  }
	  task+=WORK_SIZE;
	  work++;
	}
 
    while (result == "")
    {
        MPI_Recv(&buffor,MAX_DICTIONARY_PASSWORD,MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
		result = std::string(buffor);
				
		for(int i=0;i<MAX_DICTIONARY_PASSWORD;i++)
			buffor[i] = '\0';

		if(result != "")
			break;

		MPI_Send(&task,1,MPI_INT,status.MPI_SOURCE,WORKTAG,MPI_COMM_WORLD);
		std::cout<<"Zlecono sprawdzenie od pozycji "<<task<<" job "<<work<<" of "<<MAX_DICTIONARY_JOBS<<" ("<<(double)work/MAX_DICTIONARY_JOBS*100<<"%)"<<"\n";
		nextWorkStatus = (int)((double)work/MAX_DICTIONARY_JOBS*10000);
		if(nextWorkStatus > workStatus)
		{
			workStatus = nextWorkStatus;
			updateProgress(workStatus);
		}
		if(task >= 1327019198)
			break;
		task+=WORK_SIZE;
		work++;
    }
    if(result == "")
    {
      for(rank = 1; rank <ntasks-1; ++rank)
      {
        MPI_Recv(&buffor, MAX_DICTIONARY_PASSWORD, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		result = std::string(buffor);
		if(result != "")
			break;
		for(int i=0;i<MAX_DICTIONARY_PASSWORD;i++)
			buffor[i] = '\0';
      }
    }
    for(rank = 1; rank < ntasks; ++rank)
    {
        MPI_Send(0,0,MPI_INT,rank,DIETAG, MPI_COMM_WORLD);
    }
    if(result != "")
	{
      std::cout<<"SOLVED!!! PASSWORD IS \""<<result<<"\"!!!!\n";
	  setResult(result, difftime(time(0), begin));
	}
    else
      std::cout<<"PASSWORD NOT FOUND!\n";
    std::cout<<"It took "<<difftime(time(0), begin)<<"s to check.\n";
}

void bruteforceSlave()
{
    std::string result;
    char buffor[BRUTEFORCE_SIZE+1];
    MPI_Status status;
    long long task;
    std::string string1;
    std::string string2;
    std::string password = PASSWORD;
    std::string encryptedString;
    bool passwordCracked = false;
    
    for(;;)
    {
      	for(int i=0;i<BRUTEFORCE_SIZE+1;i++)
			buffor[i] = '\0';
        MPI_Recv(&task, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if(status.MPI_TAG == DIETAG)
            return;
	string1 = xthPermutation(task);

	for(int i=0; i<WORK_SIZE;i++)
    { 
		if(HASH == "md5")
			encryptedString = md5(string1);
		else if(HASH == "sha256")
			encryptedString = sha256(string1);
		else
		{
			encryptedString = md5(string1);
			if(encryptedString == password)
			{
				passwordCracked = true;  
				break;
			}
			encryptedString = sha256(string1);
		}
		if(encryptedString == password)
		{
			passwordCracked = true;  
			break;
		}
		string1=nextPermutation(string1);
	}

	if(passwordCracked)
		result = string1;
	else
		result = "";
        MPI_Send(result.c_str(), BRUTEFORCE_SIZE, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    }
}

void dictionarySlave()
{
	std::string result;
	long long task;
	long long counter = 0;
    MPI_Status status;
    
    std::string string1;
    std::string password = PASSWORD;
    std::string encryptedString;
    bool passwordCracked = false;
	
	std::fstream plik;
	plik.open(dictionaryPath.c_str(), std::ios::in);
	if( plik.good() == true )
	{
		for(;;)
		{
			MPI_Recv(&task, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if(status.MPI_TAG == DIETAG)
			{
				plik.close();
				return;
			}
			
			while(task > counter)
			{
				plik >> string1;
				counter++;
			}

			for(int i=0; i<WORK_SIZE;i++)
			{ 
				plik >> string1;
				task++;
				counter++;
				if(HASH == "md5")
					encryptedString = md5(string1);
				else if(HASH == "sha256")
					encryptedString = sha256(string1);
				else
				{
					encryptedString = md5(string1);
					if(encryptedString == password)
					{
						passwordCracked = true;  
						break;
					}
					encryptedString = sha256(string1);
				}
				if(encryptedString == password)
				{
					passwordCracked = true;  
					break;
				}
			}

			if(passwordCracked)
				result = string1;
			else
				result = "";
	
			MPI_Send(result.c_str(), result.size(), MPI_CHAR, 0, 0, MPI_COMM_WORLD);
		}
	} 
	else 
		std::cout << "Couldn't find dictionary on "<<system("hostname")<<"\n";  
    
}

std::string nextPermutation(std::string string)
{
  std::string nextString = string;
  int stringLength = nextString.length();
  int elementNumber = 0;
  bool changeFlag= false;
  
  for(int i = 0; i < nextString.size(); i++)
  {
    int charNumber = alphabet.findCharacter(nextString[i]);
    if(charNumber < alphabet.getLength()-1)
    {
      nextString[i] = alphabet[charNumber+1];
      changeFlag = true;
      break;
    }
    else
    {
      nextString[i] = alphabet[0];
    }
  }
  if(!changeFlag)
    nextString += alphabet[0];

  return nextString;
}

std::string xthPermutation(std::string string, unsigned int steps)	
{

   std::string xthString = string;
   int i = 0;
   int charNumber;
   
   for(int i = 0; i<steps; i++)
   {
     xthString=nextPermutation(xthString);
   }
   return xthString;
}

std::string xthPermutation(long long number)
{
   std::string xthString = "";
   std::vector<int> map;
   
   if(number > 0)
   {
	   map.push_back(number%alphabet.getLength());
	   number /= alphabet.getLength();
   }
   while(number >= 1)
   {
		number --;
		map.push_back(number%alphabet.getLength());
		number /= (alphabet.getLength());
   }
   for(int i=0;i<map.size();i++)
   {
      xthString += (alphabet[map[i]]);
   }
   return xthString;
}

void updateProgress(unsigned progress)
{
	using namespace mysqlpp;
	using namespace std;
	
	try 
	{
        Connection conn(false);
        conn.connect(DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PASS);
        Query query = conn.query();
		query << "UPDATE dbase_zadania SET progress='"<<progress<<"' WHERE ID='"<<ID<<"'";
        StoreQueryResult ares = query.store();
    } 	catch (BadQuery er) 
		{ 	// handle any connection or
			// query errors that may come up
			cerr << "Error: " << er.what() << endl;
		} 
		catch (const BadConversion& er) 
		{
			// Handle bad conversions
			cerr << "Conversion error: " << er.what() << endl <<
					"\tretrieved data size: " << er.retrieved <<
					", actual size: " << er.actual_size << endl;
		} 
		catch (const Exception& er) 
		{
			// Catch-all for any other MySQL++ exceptions
			cerr << "Error: " << er.what() << endl;
		}	
}

void setResult(std::string result, int time)
{
	using namespace mysqlpp;
	using namespace std;
	
	try 
	{
        Connection conn(false);
        conn.connect(DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PASS);
        Query query = conn.query();
		query << "UPDATE dbase_zadania SET result='"<<result<<"',time='"<<time<<"' WHERE ID='"<<ID<<"'";
        StoreQueryResult ares = query.store();
    } 	catch (BadQuery er) 
		{ 	// handle any connection or
			// query errors that may come up
			cerr << "Error: " << er.what() << endl;
		} 
		catch (const BadConversion& er) 
		{
			// Handle bad conversions
			cerr << "Conversion error: " << er.what() << endl <<
					"\tretrieved data size: " << er.retrieved <<
					", actual size: " << er.actual_size << endl;
		} 
		catch (const Exception& er) 
		{
			// Catch-all for any other MySQL++ exceptions
			cerr << "Error: " << er.what() << endl;
		}	
}

std::string sha256(const std::string str)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    std::stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ss.str();
}






















