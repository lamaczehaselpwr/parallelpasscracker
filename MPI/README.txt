Uruchomienie maszyny wirtualnej

W VirtualBoxie nale�y utworzy� now� maszyn� dla warto�ci linux - fedora 64 a nast�pnie przy wyborze dysku wybieracie plik, 
kt�ry mo�ecie pobra� tutaj: 
http://jaracza.noip.me/owncloud/index.php/s/MBqpPHyqGtFtLCT (jak trzeba b�dzie to wrzuc� tez gdzie� indziej)

Po utworzeniu maszyny wa�na jest zmiana ustawie�. 
Przydzielacie ile chcecie procesor�w, pami�ci nie trzeba du�o 256MB powinno w zupe�no�ci wystarczy�
Nale�y zmieni� w zak�adce Network adapter z NAT na Bridged Adapter i wyb�r karty sieciowej, za pomoc� kt�rej ��czycie si� z reszt� komputer�w

Je�li nie pasuje wam praca z lini komend to mo�ecie sobie zainstalowa� �rodowisko graficzne (wtedy pewnie trzeba b�dzie ustawi� wi�cej RAMu)
instrukcja instalacji �rodowiska graficznego: 
https://fedoraserver-wgblog.rhcloud.com/graphical-desktop-environments-on-fedora-21-server/

Po utworzeniu pierwszej maszyny kolejne robicie u�ywaj�c opcji Clone w VirtualBoxie, je�li chcecie testowa� kilka maszyn na jednym komputerze.
Je�li na r�nych to chyba wystarczy zrobi� tam maszyny z tego pierwszego obrazu. (Nie testowa�em wszystkich mo�liwo�ci)
na kolejnych maszynach warto te� zmieni� hostname poprzez edycj� pliku /etc/hostname


logowanie:
login: admin
has�o: pass

Aby u�ywa� nale�y ustawi� si� w folderze /home/admin/MPI (aktualny folder MPI znajduje si� na gitcie)
Komendy:
kompilacja: make

je�li macie pod��czonych kilka host�w to mo�ecie po kompilacji wywo�a� skrypt ./sendtohostsandcompile.sh
Sam skopiuje wszystkie pliki i zbuduje nawet w przypadku r�nych architektur (32/64bit) do wszystkich host�w w hostfile

uruchomienie: mpirun --hostfile hostfile haslo    (haslo to nazwa binarki z kompilacji)
dodatkowo mo�na ustawi� prze��cznik "-np 6" gdzie liczba wskazuje ile proces�w ma by� uruchomionych. 
openMPI domy�lnie chyba nie otwiera zada� dla rdzeni logicznych.
aby uruchomi� na kilku maszynach w pliku hostfile nale�y poda� w nowych liniach kolejne adresy system�w z zainstalowanym openMPI
np.

localhost
192.168.0.24 slots=4
192.168.0.25 slots=2

parametr slots okre�la ile proces�w na danej maszynie mo�na uruchomi�

sprawdzenie IP na maszynach:
ifconfig

Aby skorzysta� ze s�ownika w maszynie wirtualnej musicie ustawi� folder wsp�dzielony (ten, w kt�rym zjaduje si� s�ownik), 
najlepiej zaznaczy� �eby folder by� auto-mount, nie trzeba b�dzie si� bawi� komendami. Nast�pnie nale�y dodac admina do grupy, kt�ra mo�e
do tego folderu si� dosta�:

sudo usermod -aG vboxsf $(whoami)
reboot

Nast�pnie w folderze ~/MPI nale�y utworzy� symlinka do s�ownika

ln -s /media/sf_wybranyPrzezWasKatalog/realuniq.lst dictionary.txt

powinno dzia�a�.

Co dodatkowo wysz�o dzisiaj na zaj�ciach to fakt, �e MPI uruchamia si� na wszystkich maszynach w postaci drzewa wi�c ka�dy host powienien mie� 
dodan� reszt� do znanych host�w. Mo�na to zrobi� wywo�uj�c:

ssh ip_hosta

i nast�pnie wpisuj�c yes.

Postaram si� znale�� gdzie ustawia si� parametr, kt�ry wymusza na masterze bezpo�redni� komunikacj� bo za ka�dym w��czeniem maszyny w labach, 
zmienia si� ip i ca�y proces trzeba powt�rzy�. Ewentualnie pomy�l� nad skryptem, kt�ry to b�dzie robi�.
