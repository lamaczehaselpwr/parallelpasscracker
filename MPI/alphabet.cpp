#include "alphabet.h"
#include <iostream>
#include <unistd.h>

const char AlphabetLower[26] =
{
	'a', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u',
	'v', 'w', 'x', 'y', 'z'
};

const char AlphabetUpper[26] =
{
	'A', 'B', 'C', 'D', 'E', 'F', 'G',
	'H', 'I', 'J', 'K', 'L', 'M', 'N',
	'O', 'P', 'Q', 'R', 'S', 'T', 'U',
	'V', 'W', 'X', 'Y', 'Z'
};

const char Numbers[26] =
{
	'0', '1', '2', '3', '4', '5', '6',
	'7', '8', '9'
};

const char SpecialCharacters[33] =
{
	'!', '@', '#', '$', '%', '^', '&',
	'*', '(', ')', '-', '_', '=', '+',
	'[', ']', '{', '}', ';', ':', '\'',
	'|', ',', '.', '<', '>', '?', '/',
	'~', '`', ' ', '\"', '\\'
};
  Alphabet::Alphabet()
  {
	  
  }
  Alphabet::Alphabet(int x)
  {
    if(x & (1 << 0))
    {
      alphabet.append(AlphabetLower,26);
    }
    if(x & (1 << 1))
    {
      alphabet.append(AlphabetUpper,26);
    }
    if(x & (1 << 2))
    {
      alphabet.append(Numbers,10);
    }
    if(x & (1 << 3))
    {
      alphabet.append(SpecialCharacters,33);
    }
  }
    Alphabet::~Alphabet()
  {
	  
  }
  
  void Alphabet::initialize(int x)
  {
	alphabet = "";
	if(x & (1 << 0))
    {
      alphabet.append(AlphabetLower,26);
    }
    if(x & (1 << 1))
    {
      alphabet.append(AlphabetUpper,26);
    }
    if(x & (1 << 2))
    {
      alphabet.append(Numbers,10);
    }
    if(x & (1 << 3))
    {
      alphabet.append(SpecialCharacters,33);
    }  
  }
  char Alphabet::operator[](int element) 
  {
    if(element < alphabet.size())
      return alphabet[element];
    else
      return '\0';
  }
  
  int Alphabet::getLength()
  {
    return alphabet.size();
  }
  
  int Alphabet::findCharacter(char character)
  {
    for(int i=0; i<alphabet.size();i++)
    {
      if(alphabet[i] == character)
	return i;
    }
    return -1;
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
